import os
import matplotlib
from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans

from configuration import config
from dataset import Patchifier
from model import NeuralNet

import torch
from torch import nn
from torch.nn import functional as F
from torch import optim
from torch.utils.data import DataLoader
from torchvision import utils as vutils


# constants
use_gpu = config.getboolean("training", "use_gpu") and torch.cuda.is_available()

# paths
def mkdir(path):

    try:
        os.makedirs(path)
    except OSError:
        pass

output_path = config.get("testing", "output_path")
mkdir(output_path)

# models
encoder = NeuralNet(
    config.getint("dataset", "patch_size") * config.getint("dataset", "patch_size") * 3,
    100,
    10,
    activation=nn.Tanh
)

# load the state
state_path = config.get("testing", "state_path")
encoder.load_state_dict(torch.load(state_path))

# move to gpu if needed
if use_gpu:
    encoder = encoder.to(torch.device("cuda:0"))

data_path = config.get("testing", "data_path")

for fn in os.listdir(data_path):
    print("processing {}".format(fn))

    path = os.path.join(data_path, fn)
    
    image_output_path = os.path.join(output_path, fn.split(".")[0])
    mkdir(image_output_path)

    # dataset
    dataset = Patchifier(
        path,
        int(config.get("dataset", "patch_size")),
        whiten=config.getboolean("dataset", "whiten"),
        stride=int(config.get("testing", "stride")),
        sample_rate=config.getfloat("testing", "sample_rate")
    )

    dataloader = DataLoader(
        dataset,
        batch_size=int(config.get("testing", "batch_size")),
        shuffle=config.getboolean("dataset", "shuffle"),
        num_workers=int(config.get("training", "num_workers"))
    )

    image_activations = torch.FloatTensor([])

    for batch_id, data in enumerate(dataloader, 0):
        current_batch_size = data.size(0)        

        if use_gpu:
            data = data.to(torch.device("cuda:0"))


        # push data through model
        with torch.no_grad():
            encoded = encoder(data.view(current_batch_size, -1))

            image_activations = torch.cat([image_activations,
                                           encoded.detach().cpu()], dim=0)
            #print("image_activations", image_activations.size())

    image_activations_np = image_activations.numpy()

    activation_path = "{}/activations.npy".format(image_output_path)
    np.save(activation_path, image_activations_np)

    print("saved activations to {}".format(activation_path))

    print("clustering activations (may take some time)")
    kmeans = KMeans(n_clusters=config.getint("testing", "n_clusters"))
    labels = kmeans.fit_transform(image_activations_np)
    label_path = "{}/labels.npy".format(image_output_path)
    np.save(label_path, labels)

    print("finished processing image {}".format(fn))