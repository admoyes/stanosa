import os
from configparser import ConfigParser

current_path = "/".join(str(__file__).split("/")[:-1])
config_path = os.path.join(current_path, "stanosa.conf")

config = ConfigParser()

with open(config_path, "r") as f:
    config.read_file(f)
